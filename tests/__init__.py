import logging
import os

logging.disable(logging.CRITICAL)

skip_integration_tests = False if os.environ.get('ZAPPIX_TEST_INTEGRATION', 'no').upper() == 'YES' else True
skip_on_gitlab = True if os.environ.get('GITLAB_CI', '') else False

zabbix_agent_address = os.getenv('ZAPPIX_AGENT', 'zabbix-agent')
zabbix_server_address = os.getenv('ZAPPIX_SERVER', 'zabbix-server')
zabbix_api_address = os.getenv('ZAPPIX_API', 'http://zabbix-server')
zabbix_default_user = os.getenv('ZAPPIX_API_USER', 'Admin')
zabbix_default_password = os.getenv('ZAPPIX_API_PASS', 'zabbix')
